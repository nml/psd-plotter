Matlab 2017 
% psd plotter
Input arguments: (tail, total, nbins, tail_lim, total_lim, mode, coeffs)

% tail is an array with the integral of the pulse tail

% total is an array with the integral of the total pulse

% nbins is the number of bins of the scatter plot, the data will be

%     histogrammed in a nbins x nbins matrix

% tail_lim =[min max] hist and plot limits for tail integral, can be []

% total_lim =[min max] hist and plot limits for total integral, can be []

% mode = 0 tail VS total
% mode = 1 ratio VS total

% coeffs are PSD coefficients in quadratic equation

%%%%%%%%% set the data
% max_tail and max_total useful is there are outliers
% if not leave them blank